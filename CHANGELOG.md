# CHANGELOG

## 1.0.13 (2025-02-14)

### New

*   Added the `mix_colors` function.

## 1.0.12 (2024-11-13)

### Fixed

*   Added distribution statement to LICENSE.

## 1.0.11 (2024-11-11)

### New

*   Added file-level docs.

## 1.0.10 (2024-11-11)

### Fixed

*   Fill areas that extend beyond view box.

## 1.0.9 (2024-09-16)

### Fixed

*   Incorrect implementation of row-major legend order.

## 1.0.8 (2024-09-15)

### Fixed

*   Incorrect grid lines for logarithmic scaling.

## 1.0.7 (2024-08-19)

### Fixed

*   Removed metadata from `tkz.py` in favor of `pyproject.toml` metadata.

### Added

*   CHANGELOG.md
